<?php
/**
 * Test Platform API
 * - This file contains common functions used to test google transalte API
 *
 * @author Yen Chen <yen.ycc.chen@gmail.com>., 2015
 */
// ===========================================================
// header
// ===========================================================
if (! isset($absolute_path))
    if (strpos(strtolower(PHP_OS), "win") !== false)
	define('ABSOLUTE_PATH', "C:\\translation-testing\\src\\testplatform");
    else
	define('ABSOLUTE_PATH', "/translation-testing/src/testplatform");
if (! file_exists(ABSOLUTE_PATH . DIRECTORY_SEPARATOR . "require.inc.php"))
    die($_SERVER['PHP_SELF'] . ": error: cannot find require.inc.php! cannot continue.");
else
    require (ABSOLUTE_PATH . DIRECTORY_SEPARATOR . "require.inc.php");

// ===========================================================
// functions
// ===========================================================
/**
 * Displays the program usage.
 *
 * @access local
 */
function DebugPause()
{
    $output = "";
    print CON_COLOR_GREEN . "Program Pause!!!" . CON_COLOR_NORMAL ."Press enter key to continue...";
    # get input hiding the output
    $temp = fopen ("php://stdin","r");
    $output = chop($output = fgets($temp));
    print "\n";
}
function Usage()
{
    print "Usage of Test platform for Google Translate API\n\n";
    print "Execution: \n";
    print "\t" . $_SERVER['_'] . " " . $_SERVER['PHP_SELF'] ."\n\n";
    print "Configuration: \n";
    print "\tA configuration file with a valid testcase/google API key must be supplied\n";
    print "\tDefault working directory for the config file location: " . CONFIG_PATH . DIRECTORY_SEPARATOR . "Config.xml" . "\n\n";
    print "Valid options:\n";
    print "\t-h | --help         \t Help. Prints this usage message\n";
    ExitProper(1);
}
function ExitProper($exit_code)
{
    print CON_COLOR_GREEN . "Program Completed." . CON_COLOR_NORMAL . "\n";
    exit($exit_code);
}
/**
 * Loads the configuration data XML.
 *
 * @param $config_file Relative
 * @return array Return result.
 * @throws InvalidOperationException
 * @throws FileHandleException
 */
function LoadConfiguration($config_file)
{
    $config_file_handle = NULL;
    $file_path = CONFIG_PATH . "/" . $config_file;

    if (! ($config_file_handle = fopen($file_path, "r")))
        throw new FileHandleException("Could not open \"" . $file_path . "\".");
    if (! is_resource($config_file_handle))
        throw new FileHandleException("Could not open \"" . $file_path . "\".");
        
    // make sure the config file has some content
    if (! filesize(CONFIG_PATH . "/" . $config_file) > 0)
	throw new InvalidOperationException("Configuration file is invalid or corrupt!");
    else
	$config_file_content = fread($config_file_handle, filesize(CONFIG_PATH . "/" . $config_file));
    
    $config_xml = new SimpleXMLElement($config_file_content);
    fclose($config_file_handle);
    
    // generate the credentials from the config
    if (count($config_xml->configuration) > 0)
    {
        $config_url = trim($config_xml->configuration->google['url']);
        $config_key = trim($config_xml->configuration->google['key']);

	if (count($config_xml->configuration->testcase) > 0) {
	    foreach($config_xml->configuration->testcase as $index => $case){
		$testsuite[] = get_object_vars($case)['@attributes'];
	    }
	}
    }
    else
    {
        print_r(CON_COLOR_LIGHTRED . "Configuration XML is missing the \"configuration\" section! This is a fatal error and we cannot continue!" . CON_COLOR_NORMAL);
        throw new FileHandleException("Configuration XML is missing the \"configuration\" section!");
        return array(
            'google_url' => null,
            'google_key' => null,
            'testsuite' => null,
        );
    }
    return array(
	'google_url' => $config_url,
	'google_key' => $config_key,
	'testsuite' => $testsuite
    );
}
$config_file = DEF_CONF_FILE;
$short_opts = "h";
$long_opts = array(
    "help"
);
$options = getopt($short_opts, $long_opts);
if(!empty($options)){
    Usage();
}




// ===========================================================
// Main
// ===========================================================
try
{
    $config_xml = LoadConfiguration($config_file);

    include (TESTCASE_PATH . DIRECTORY_SEPARATOR . "class.testcase.inc.php");
    foreach($config_xml['testsuite'] as $testsuite_id => $testsuite) {
	$__mycase = new TestCase($config_xml['google_url'], $config_xml['google_key'], $testsuite['action'], $testsuite['query'] ."," . $testsuite['target'] . "," . $testsuite['source']);
	$__mycase->_testcase_return = $__mycase->Run();
	$__mycase->Verify();
	$__mycase_status = $__mycase->GetStatus();
	$__mycase_return = $__mycase->GetTestResult();

	print_r("Google API method:" . $testsuite['action'] . "\tquery:" . $testsuite['query'] . "\ttarget:" . $testsuite['target'] . "\tsource:" . $testsuite['source'] . "\n");
	switch ($__mycase_status)
	{
	    case EXT_TESTCASE_FAIL: {
		print_r("TEST RESULT: " . CON_COLOR_LIGHTRED . "FAILED" . CON_COLOR_NORMAL . "\n");
		break;
	    }
	    case EXT_TESTCASE_PASS: {
		print_r("TEST RESULT: " . CON_COLOR_GREEN . "PASSED" . CON_COLOR_NORMAL . "\n");
		break;
	    }
	    case EXT_TESTCASE_UNKNOW: {
		print_r("TEST RESULT: " . CON_COLOR_BLUE . "NOT RUN" . CON_COLOR_NORMAL . "\n");
		break;
	    }
	}
    }
}
catch (Exception $e)
{
    print_r($e->GetMessage());
    print_r("Failed to load server configuration XML successfully!");
    print_r($e);
    ExitProper(3);
}
// vim: ts=4 sw=4
?>
