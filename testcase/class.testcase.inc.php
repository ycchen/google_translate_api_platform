<?php
/*
*   Test cases:
*   ---------------------------------------------------------
*
*   Written by:
*   - Yen Chen <yen.ycc.chen@gmail.com>., 2015
*/
define('TESTCASE_UNKNOWN', 0);
define('TESTCASE_PASSED', 1);
define('TESTCASE_FAILED', -1);
define('TESTCASE_Translate', 'translate');
define('TESTCASE_Detect', 'detect');
define('TESTCASE_LanguageSupport', 'languagesupport');

// ===========================================================
//  Wrapper class
// ===========================================================
class TestCase
{
    private $_testcase_status;
    private $_testcase_verify_list;
    public function __construct($_googleurl, $_googlekey, $action, $parameter = null)
    {
	$this->_testcase_result = TESTCASE_UNKNOWN;
	$this->_testcase_return = '';
	$this->__googleurl = $_googleurl;
	$this->_googlekey = $_googlekey;
	$this->_action = $action;
	$this->_parameter = $parameter;
    }
    /**
    * This function actually runs the test case.
    *
    * Run()
    */
    public function Run()
    {
	$this->_GoogleTranslate = new GoogleTranslate($this->__googleurl, $this->_googlekey);
	$q_parameter = $this->ParseTestData($this->_parameter);
	switch ($this->_action)
	{
	    case TESTCASE_Translate: {
		$this->_testcase_return = $this->_GoogleTranslate->gTranslateText($q_parameter['q'], $q_parameter['target'], $q_parameter['source']);
		break;
	    }
	    case TESTCASE_Detect: {
		$this->_testcase_return = $this->_GoogleTranslate->gDetect($q_parameter['q'], $q_parameter['target'], $q_parameter['source']);
		break;
	    }
	    case TESTCASE_LanguageSupport: {
		$this->_testcase_return = $this->_GoogleTranslate->gLanguageSupport($q_parameter['target']);
		break;
	    }
	}
	return $this->_testcase_return;
    }
    /**
     * This function verifies the test case after execution.
     *
     * Verify()
     */
    public function Verify()
    {
	$q_parameter = $this->ParseTestData($this->_parameter);
	switch ($this->_action)
	{
	    case TESTCASE_Translate: {
		if(is_null($q_parameter['source'])) {
		    $detectedSourceLanguage = $this->_GoogleTranslate->GetTestResultLanguageType();
		    $this->_testcase_verify_list = $this->_GoogleTranslate->gTranslateText($this->_testcase_return, (string)$detectedSourceLanguage);
		}
		else {
		    $this->_testcase_verify_list = $this->_GoogleTranslate->gTranslateText($this->_testcase_return, $q_parameter['source'], $q_parameter['target']);
		}
		if($this->_testcase_verify_list === $q_parameter['q'])
		    $status = TESTCASE_PASSED;
		else
		    $status = TESTCASE_FAILED;

		break;
	    }
	    case TESTCASE_Detect: {
		$status = TESTCASE_PASSED;
		if(count($q_parameter['q']) != count($this->_testcase_return))
		    $status = TESTCASE_FAILED;

		break;
	    }
	    case TESTCASE_LanguageSupport: {
		$status = TESTCASE_PASSED;
		if(!is_null($q_parameter['target'])) {
		    foreach($this->_testcase_return as $key => $value) {
			if(empty($value->language) || empty($value->name)) {
			    $status = TESTCASE_FAILED;
			    break;
			}
		    }
		}
		else {
		    foreach($this->_testcase_return as $key => $value) {
			if(empty($value->language)) {
			    $status = TESTCASE_FAILED;
			    break;
			}
		    }
		}
		if(count($this->_testcase_return) != 91)
		    $status = TESTCASE_FAILED;

		break;
	    }
	}
	$this->SetStatus($status);
    }
    /**
     * Returns the testcase testing result.
     *
     * GetStatus()
     * @returns int: Current execution status of test case
     */
    public function GetTestResult()
    {
	return $this->_testcase_return;
    }
    /**
     * Returns the testcase status.
     *
     * GetStatus()
     * @returns int: Current execution status of test case
     */
    public function GetStatus()
    {
	return $this->_testcase_status;
    }
    /**
     * Sets the testcase status.
     *
     * SetStatus($status)
     * @param $status int: Test case execution status value
     */
    public function SetStatus($status)
    {
	$this->_testcase_status = $status;
    }
    /**
     * Parse Test parameter information
     * ParseTestData()
     *
     * @param array $parse_data
	 contain test method/test parameter
     * @return mixed array|null
     */
    private function ParseTestData($parse_data)
    {
	$para_list = explode(",", $parse_data);
	foreach($para_list as $para_index => $para_val) {
	    switch ($para_index)
	    {
		case 0:{
		    $buildlist['q'] = (empty($para_val)) ? null : explode("/", $para_val);
		    break;
		}
		case 1:{
		    $buildlist['target'] = (empty($para_val)) ? null : $para_val;
		    break;
		}
		case 2:{
		    $buildlist['source'] = (empty($para_val)) ? null : $para_val;
		    break;
		}
	    }
	}
	return $buildlist;
    }
}
# vim: ts=4 sw=4
?>
