<?php
/**
 * General Common Require
 *
 */
// ===========================================================
// constants
// ===========================================================
define('CONFIG_PATH', ABSOLUTE_PATH . DIRECTORY_SEPARATOR .  "config");
define('INCLUDE_PATH', ABSOLUTE_PATH . DIRECTORY_SEPARATOR .  "include");
define('TESTCASE_PATH', ABSOLUTE_PATH . DIRECTORY_SEPARATOR .  "testcase");
define('EXCEPTION_PATH', INCLUDE_PATH . DIRECTORY_SEPARATOR . "Exceptions");
define('DEF_CONF_FILE', "Config.xml");

/*
 * External Testcase Return Codes
 */
define('EXT_TESTCASE_UNKNOW', 0);
define('EXT_TESTCASE_FAIL', -1);
define('EXT_TESTCASE_PASS', 1);

// ===========================================================
// includes
// ===========================================================
// main low-level framework
// Base RESTful classes
require_once (INCLUDE_PATH . DIRECTORY_SEPARATOR .  "class.GoogleTranslatorRESTful.inc.php");

// platform system-wide generic global exceptions.
require_once (EXCEPTION_PATH . DIRECTORY_SEPARATOR . "class.FileHandleException.inc.php");
require_once (EXCEPTION_PATH . DIRECTORY_SEPARATOR . "class.InvalidOperationException.inc.php");

// color log
define("CON_COLOR_BLUE", "\033[00;34m");
define("CON_COLOR_GREEN", "\033[00;32m");
define("CON_COLOR_LIGHTRED", "\033[00;31m");
define("CON_COLOR_NORMAL", "\033[00;39m");
?>
