<?php
/**
 * Google Translate API RESTful Call Library
 * - This class implements common functions required to perform RESTful calls against a Google Translate API.
 *
 * @author Yen Chen <yen.ycc.chen@gmail.com>., 2015
 */
class GoogleTranslate {
    /**
     * Google URL API
     *  @var string
     * Access Key to API
     *  @var string 
     * HTTP Curl
     *  @var Curl
     * list parameters used in get request
     *  @var array
     */
    private $apiUrl = '';
    private $accessKey = "";
    private $connecturl;
    private $parameters = array();

    /**
     * Service method translate, detect and language support
     */
    CONST SERVICE_TRANSLATE = 'translate';
    CONST SERVICE_DETECT = 'detect';
    CONST SERVICE_LANGUAGE = 'language';

    public function __construct($apiUrl, $accessKey) {
	$this->setAPIUrl($apiUrl);
	$this->setAccessKey($accessKey);
    }
    /**
     * Set access key
     * @param string $key 
     */
    public function setAccessKey($key) {
	if (strlen($key) == 39) {
	    $this->accessKey = $key;
	} else {
	    throw new GoogleTranslateInvalidKeyException();
	}
    }
    /**
     * Set API Url
     * @param string $ri 
     */
    public function setAPIUrl($url) {
	if (!is_null($url)) {
	    $this->apiUrl  = $url;
	} else {
	    throw new GoogleTranslateInvalidUrlException();
	}
    }

    /**
     * Translate text
     * @param string/array  $textThe
	 text to be translated
     * @param string	    $targetLanguage
	 The language to translate the source text into
     * @param string/null   $sourceLanguage
	 The language of the source text. If a language is not provided, the system is auto-detected  source test
     */
    public function gTranslateText($text, $targetLanguage, $sourceLanguage = null) {
	if ($this->isValid($text, $targetLanguage, $sourceLanguage)) {
	    reset($text);

	    //build input parameters 
	    $this->addQueryParam('key', $this->accessKey);
	    $this->addQueryParam('q', $text);
	    $this->addQueryParam('target', $targetLanguage);

	    if(!is_null($sourceLanguage)){
		$this->addQueryParam('source', $sourceLanguage);                
	    }

	    //Request Google API connect
	    $result = $this->RequestAPI();

	    if (!is_array($text)) {
		//parse returns
		$result = current($result->translations);
		if (isset($result->detectedSourceLanguage)) {
		    $sourceLanguage = $result->detectedSourceLanguage;
		}
		//return translate
		$this->detectedSourceLanguage = $sourceLanguage;
		return $result->translatedText;
	    } else {
		//this is multiple text
		//parse returns
		$result = $result->translations;
		$arrTranslateReturn = array();
		$arrSourceReturn = array();
		foreach ($result as $itemResult) {
		    $arrTranslateReturn[] = $itemResult->translatedText;
		    if (isset($itemResult->detectedSourceLanguage)) {
			$arrSourceReturn[] = $itemResult->detectedSourceLanguage;
		    }
		}

		//return by reference the language in case detected language
		$sourceLanguage = $arrSourceReturn;
		$this->detectedSourceLanguage = $sourceLanguage;
		return $arrTranslateReturn;
	    }
	} else {
	    return false;
	}
    }
    /**
     * Deletect Language
     * @param string    $text
	 The text or list the text to be detect
     * @param reference $isReliable
	 check is reliable flag
     * @return string|array language or list of language detected 
     */
    public function gDetect($text, &$isReliable = null) {
	if ($this->isValid($text, null, null, false)) {
	    reset($text);

	    //build input parameters 
	    $this->addQueryParam('key', $this->accessKey);
	    $this->addQueryParam('key', $this->accessKey);
	    $this->addQueryParam('q', $text);
					    
	    //Request Google API connect
	    $result = $this->RequestAPI(self::SERVICE_DETECT);
					
	    if (count($result->detections) == 1) {
		//parse returns
		$result = current(current($result->detections));
		$isReliable = $result->isReliable;

		//return translate
		return $result->language;
	    } else {
		//this is multiple text
		//parse returns
		$result = $result->detections;

		$arrIsReliable = array();
		$arrSourceReturn = array();
		//get translates
		foreach ($result as $itemResult) {
		    $itemResult = current($itemResult);
		    $arrIsReliable[] = $itemResult->isReliable;
		    $arrSourceReturn[] = $itemResult->language;
		}
		//return by reference the language is realiable
		$isReliable = $arrIsReliable;
		//return list of detect language
		return $arrSourceReturn;
	    }
	} else {
	    return false;
	}
    }
    /**
     * Language support
     * @param string|null $target language target
     */
    public function gLanguageSupport($target = null) {
	//build input parameters 
	$this->addQueryParam('key', $this->accessKey);
	if($target != null) {
	    if ($this->validLanguage($target))
		$this->addQueryParam('target', $target);                
	    else
		return false;
	}

	//Request Google API connect
	$result = $this->RequestAPI(self::SERVICE_LANGUAGE);

	//parse returns
	$result = $result->languages;        
	return $result;
    }
    /**
     * Check input validation 
     * @param string/array	$text
	 The text or list the text to be validate
     * @param string	$targetLanguage
	 target language to be validate
     * @param string	$sourceLanguage
	 source language to be validate
     * @param boolean	$targetRequired
	 the target language is required
     * @return boolean 
     */
    private function isValid(&$text, $targetLanguage = null, $sourceLanguage = null, $targetRequired = true) {
	//in case of numeric only in text, return false
	if (!is_array($text)) {
	    $text = array($text);
	}
	//valid list of translate
	foreach ($text as $keyText => $oneText) {
	    //check is numeric
	    if (is_numeric($oneText) || strlen($oneText) < 2) {
		//remove from list translate
		unset($text[$keyText]);
	    }
	}
	//no text valid
	if (count($text) <= 0)
	    return false;
	//target required
	if ($targetRequired) {
	    //is valid language target
	    if (!$this->validLanguage($targetLanguage))
		return false;
	}
	//is valid language source
	if (!is_null($sourceLanguage)) {
	    if (!$this->validLanguage($sourceLanguage))
		return false;
	}
	return true;
    }
    /**
     * validate language
     * @param string $lang language text to be validate
     * @return boolean 
     */
    private function validLanguage($lang) {
	$regexpValidLanguage = '%([a-z]{2})(-[a-z]{2})?%';
	return (preg_match($regexpValidLanguage, $lang) == 0) ? false : true;
    }
    /**
     * Send a raw HTTP request to a RESTful server
     *
     * @param google translate api method
     * @param string $post_type            
     * @return mixed array|null
     * @throws google Translate Expection
     * @throws Exception
     */
    private function RequestAPI($service = self::SERVICE_TRANSLATE) {
	//choose service
	$ch = curl_init();
	$opts = array(
	    CURLOPT_HTTPHEADER => array("Content-Type: application/json; charset=UTF-8"),
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_CUSTOMREQUEST => "GET"
	);
	switch ($service) {
	    case self::SERVICE_DETECT : $url = $this->apiUrl . '/detect';
		break;
	    case self::SERVICE_LANGUAGE : $url = $this->apiUrl . '/languages';
		break;
	    case self::SERVICE_TRANSLATE : default: $url = $this->apiUrl;
		break;
	}
	//case exists parameters, add to url
	if (count($this->parameters) > 0) {
	    $url .= '?';
	    //add for each item 

	    foreach ($this->parameters as $keyParam => $param) {
		//used in case de multiple text translate
		if (is_array($param)) {
		    foreach ($param as $subParam) {
			$url .= rawurlencode($keyParam) . '=' . rawurlencode($subParam) . "&";
		    }
		} else {
		    $url .= rawurlencode($keyParam) . '=' . rawurlencode($param) . "&";
		}
	    }
	    $url = rtrim($url, " &");
	}

	//init curl
	$opts[CURLOPT_URL] = $url;
	curl_setopt_array($ch, $opts);

	//execute curl
	$cr = curl_exec($ch);

	//parse curl
	$response['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$response['body'] = json_decode($cr);

	//close curl
	curl_close($ch);
	if ($response['http_code'] == 404)
	    throw new GoogleTranslateNotFoundException();
	if ($response['http_code'] == 400)
	    throw new GoogleTranslateBadRequestException();
	if (($response['http_code'] == 200 || $response['http_code'] == 304) && !isset($response->error))
	    return $response['body']->data;
	else
	    throw new GoogleTranslateInvalidBehaviorException();

    }
    /**
     * Add Query param in connect
     * @param type $key
     * @param type $value 
     */
    private function addQueryParam($key, $value) {
	//remove possible whitespaces, utf8 encode AND add to params list
	if (is_array($value)) {
	    foreach ($value as $keyValue => $itemValue) {
		$value[$keyValue] = utf8_encode($itemValue);
	    }
	} else
	    $value = utf8_encode($value);

	//add to param list
	$this->parameters[utf8_encode(str_replace(' ', '', $key))] = $value;
    }
    /**
     * Return Google API Test Result Language type
     */
    public function GetTestResultLanguageType() {
	return $this->detectedSourceLanguage[0];
    }
}
/**
 * Google Translate Exception Invalid Access Key
 */
class GoogleTranslateInvalidKeyException extends Exception {
    function __construct() {
	parent::__construct('Invalid Access Key');
    }
}
/**
 * Google Translate Exception Not found 404, probable problem in connect internert
 */
class GoogleTranslateNotFoundException extends Exception {
    function __construct() {
	parent::__construct('Not Found Request', 404);
    }
}
/**
 * Google Translate Exception Bad Request 400, problemw with url queary data
 */
class GoogleTranslateBadRequestException extends Exception {
    function __construct() {
	parent::__construct('Bad Request', 400);
    }
}
/**
 * Google Translate Exception Incorrecet Behavior, 
 */
class GoogleTranslateInvalidBehaviorException extends Exception {
    function __construct() {
	parent::__construct('Invalid Behavior', 400);
    }
}
