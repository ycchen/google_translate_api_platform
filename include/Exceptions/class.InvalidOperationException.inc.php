<?php
/**
 * Invalid Operation Exception Class
 * - This class implements a standard exception used to indicate invalid operations.
 *
 */
define('INC_INVALID_OPERATION_EXCEPTION', TRUE);

// ===========================================================
// InvalidOperationException class
// ===========================================================
class InvalidOperationException extends Exception
{

    /**
     * Methods
     */
    /**
     * Throws an invalid operation exception.
     *
     * @param string $message
     *            Exception message
     * @param int $code
     *            Exception error code
     * @param Exception $previous
     *            Previous exception (if any)
     */
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
    
    // stub
}

// vim: ts=4 sw=4
?>
