<?php
/**
 * File Handle Exception Class
 * - This class implements a standard exception used for file handle errors.
 *
 */
define('INC_FILE_HANDLE_EXCEPTION', TRUE);

// ===========================================================
// FileHandleException class
// ===========================================================
class FileHandleException extends Exception
{

    /**
     * Methods
     */
    /**
     * Throws a file handle exception.
     *
     * @param string $message
     *            Exception message
     * @param int $code
     *            Exception error code
     * @param Exception $previous
     *            Previous exception (if any)
     */
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
    
    // stub
}

// vim: ts=4 sw=4
?>
